package com.tu.cgipriademo.fields;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class FieldsAdjacencyCheckerTests {
	private ApplicationData applicationData;
	private FieldsAdjacencyChecker checker;
	
	@BeforeEach
	private void setUp() {
		this.applicationData = (new ApplicationService().readApplicationData("./src/test/maaalad_orig.csv"));
		this.checker = new FieldsAdjacencyChecker(applicationData);
	}
	
	@Test
	void checkAdjecency_correctThroughMultipleFields() {
		
		assertTrue(checker.checkAdjecency(8600805, 15228401, applicationData.get(8600805).getAreaFields()));
		
		applicationData.get(8600805).getAreaFields().remove(Long.valueOf(8600804));		
		assertFalse(checker.checkAdjecency(8600805, 15228401, applicationData.get(8600805).getAreaFields()));
		
		assertTrue(checker.checkAdjecency(19200601, 20448901, applicationData.get(19200601).getAreaFields()));
		
		applicationData.get(19200601).getAreaFields().remove(Long.valueOf(20384401));	
		applicationData.get(19200601).getAreaFields().remove(Long.valueOf(8600814));
		assertFalse(checker.checkAdjecency(19200601, 20448901, applicationData.get(19200601).getAreaFields()));
		
	}
	
	
	@Test
	void checkFieldsAdjacency_returnsNonAdjacentFields() {
		assertEquals(0, checker.checkFieldsAdjacency(8600805, applicationData.get(8600805).getAreaFields()).size());
		applicationData.get(8600805).getAreaFields().remove(Long.valueOf(8600804));	
		
		List<Long> nonAdjacent = checker.checkFieldsAdjacency(8600805, applicationData.get(8600805).getAreaFields());
		assertTrue(nonAdjacent.contains(Long.valueOf(15228401)));
				
		assertEquals(0, checker.checkFieldsAdjacency(19200601, applicationData.get(19200601).getAreaFields()).size());
		applicationData.get(19200601).getAreaFields().remove(Long.valueOf(8600814));		
		assertEquals(0, checker.checkFieldsAdjacency(19200601, applicationData.get(19200601).getAreaFields()).size());
		applicationData.get(19200601).getAreaFields().remove(Long.valueOf(20384401));	
		assertEquals(2, checker.checkFieldsAdjacency(19200601, applicationData.get(19200601).getAreaFields()).size());
	
	}
	
	
	
}
