package com.tu.cgipriademo.fields;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;


class ApplicableFieldsServiceTests {
	
	private Field[] fields;
	private float[] fieldSizes = {0.1f, 0.12f, 3.5f, 3.9f};	
	private ApplicationData appData;
	private ApplicableFieldsService applicableFieldsService;
	
	@BeforeEach
	void setUp() throws Exception {
		applicableFieldsService = new ApplicableFieldsService();
		this.appData = Mockito.mock(ApplicationData.class);
		this.fields = new Field[4];
		for(int i = 0; i < 4; i++) {
			
			this.fields[i] = Mockito.mock(Field.class);
			when(appData.get(i)).thenReturn(this.fields[i]);
			when(fields[i].getFieldSize()).thenReturn(this.fieldSizes[i]);
			when(fields[i].getAreaNr()).thenReturn(1);
			when(fields[i].isActive()).thenReturn(true);
		}
	}

	@Test
	void checkFieldSizeRequirement_returnsEmptyWhen1FieldAndBig() {
		Collection<Long> fieldIDs = new ArrayList<Long>();
		fieldIDs.add(Long.valueOf(2));
		assertTrue(applicableFieldsService.checkFieldSizeRequirement(fieldIDs, appData).isEmpty());
	}
	
	@Test
	void checkFieldSizeRequirement_returnsEmptyWhen1FieldAndSmall() {
		Collection<Long> fieldIDs = new ArrayList<Long>();
		fieldIDs.add(Long.valueOf(0));
		assertTrue(applicableFieldsService.checkFieldSizeRequirement(fieldIDs, appData).isEmpty());
	}
	
	@Test
	void checkFieldSizeRequirement_returnsEmptyWhenAllSmaller() {
		Collection<Long> fieldIDs = new ArrayList<Long>();
		fieldIDs.add(Long.valueOf(0));
		fieldIDs.add(Long.valueOf(1));
		
		assertTrue(applicableFieldsService.checkFieldSizeRequirement(fieldIDs, appData).isEmpty());
	}
	
	@Test
	void checkFieldSizeRequirement_returnsEmptyWhenAllBigger() {
		Collection<Long> fieldIDs = new ArrayList<Long>();
		fieldIDs.add(Long.valueOf(2));
		fieldIDs.add(Long.valueOf(3));
		
		assertTrue(applicableFieldsService.checkFieldSizeRequirement(fieldIDs, appData).isEmpty());
	}
	
	@Test
	void checkFieldSizeRequirement_returnsAllIfBiggerExists() {
		Collection<Long> fieldIDs = new ArrayList<Long>();
		fieldIDs.add(Long.valueOf(0));
		fieldIDs.add(Long.valueOf(1));
		fieldIDs.add(Long.valueOf(2));
		fieldIDs.add(Long.valueOf(3));
		
		assertTrue(applicableFieldsService.checkFieldSizeRequirement(fieldIDs, appData).size() == 4);
	}
	
	
	@Test
	void findApplicableAreaFields_callsOnFieldsAreaFields() {
		List<Long> areaFields = new ArrayList<Long>();
		
		areaFields.add(Long.valueOf(1));
		areaFields.add(Long.valueOf(2));
		
		when(this.fields[0].getAreaFields()).thenReturn(areaFields);
		when(this.appData.hasKey(anyLong())).thenReturn(true);
		Collection<Long> s = this.applicableFieldsService.findApplicableAreaFields(this.fields[0], appData);
		
		verify(this.fields[1]).isActive();
		
		assertTrue(s.size() == 0);		
		
	}

}
