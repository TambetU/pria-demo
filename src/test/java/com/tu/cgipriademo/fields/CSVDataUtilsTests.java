package com.tu.cgipriademo.fields;

import static org.junit.jupiter.api.Assertions.*;


import java.util.ArrayList;
import java.util.List;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.junit.jupiter.MockitoExtension;

import com.tu.cgipriademo.csv.CSVDataUtils;

@ExtendWith(MockitoExtension.class)
class CSVDataUtilsTests {
	@Test
	void parseFieldIDList_parsesPopulatedList() {
		String s = "123, 4, 3, 2123";
		
		List<Long> fieldIDs = CSVDataUtils.parseFieldIDList(s);	
		assertEquals(4,fieldIDs.size());
		assertTrue(fieldIDs.contains(Long.valueOf(123)));
		assertTrue(fieldIDs.contains(Long.valueOf(4)));
		assertTrue(fieldIDs.contains(Long.valueOf(3)));
		assertTrue(fieldIDs.contains(Long.valueOf(2123)));		
	}
	
	@Test
	void parseFieldIDList_parsesTrailingComma() {
		String s = "123, 4, 3, 2123,";
		
		List<Long> fieldIDs = CSVDataUtils.parseFieldIDList(s);	
		assertEquals(4,fieldIDs.size());	
		assertTrue(fieldIDs.contains(Long.valueOf(2123)));
	}
	
	@Test
	void parseFieldIDList_returnsEmptyListIfEmpty() {
		String s = "";
		
		List<Long> fieldIDs = CSVDataUtils.parseFieldIDList(s);	
		assertEquals(new ArrayList<Long>(), fieldIDs);	
	}
	
	@Test
	void fieldIDstoString_stringsPopulatedList() {
		List<Long> ids = new ArrayList<Long>();
		ids.add(Long.valueOf(4));
		ids.add(Long.valueOf(5));
		ids.add(Long.valueOf(6));
		ids.add(Long.valueOf(120));
		
		assertEquals("4,5,6,120",CSVDataUtils.fieldIDstoString(ids));
		
	}
	
	@Test
	void fieldIDsToString_returnsEmptyStringWhenEmpty() {
		assertEquals("", CSVDataUtils.fieldIDstoString(new ArrayList<Long>()));
	}
	
	@Test
	void fieldNrsToString_returnsEmptyWhenEmpty() {
		assertEquals("", CSVDataUtils.fieldNrsToString(new ArrayList<FieldNr>()));
	}
	
	@Test
	void fieldNrsToString_stringsPopulatedList() {
		List<FieldNr> nrs = new ArrayList<FieldNr>();
		nrs.add(FieldNr.fromString("1-1"));
		nrs.add(FieldNr.fromString("1-3"));
		nrs.add(FieldNr.fromString("7"));
		assertEquals("1-1,1-3,7", CSVDataUtils.fieldNrsToString(nrs));
	}
}
