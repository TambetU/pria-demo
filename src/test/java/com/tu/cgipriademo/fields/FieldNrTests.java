package com.tu.cgipriademo.fields;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;

class FieldNrTests {

	@Test
	void compareTo_WorksOnPrimaries() {
		FieldNr f1 = FieldNr.fromString("25");
		FieldNr f2 = FieldNr.fromString("26-1");
		
		assertEquals(f1.compareTo(f2), Integer.compare(25,26));
	}
	
	@Test
	void compareTo_WorksOnSecondary() {
		FieldNr f1 = FieldNr.fromString("25-4");
		FieldNr f2 = FieldNr.fromString("25-12");
		
		assertEquals(f1.compareTo(f2), Integer.compare(4,12));
	}
	
	@Test
	void compareTo_worksSorting() {
		FieldNr[] fields = {FieldNr.fromString("1-7"), FieldNr.fromString("1-11"), FieldNr.fromString("2"),FieldNr.fromString("2-1")};
		
		List<FieldNr> fieldsToSort = new ArrayList<FieldNr>(Arrays.asList(
					fields[2],
					fields[3],
					fields[0],
					fields[1]
				));
		
		Collections.sort(fieldsToSort);
		
		assertIterableEquals(Arrays.asList(fields), fieldsToSort);		
	}
	
	
}
