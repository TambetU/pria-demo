package com.tu.cgipriademo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


import com.tu.cgipriademo.fields.ApplicationData;
import com.tu.cgipriademo.fields.ApplicationService;


/**
 * Main, runs the program
 * @author Tambet
 *
 */
@SpringBootApplication
public class CgipriademoApplication implements CommandLineRunner{
	
	@Autowired
	ApplicationService applicationService;
	
	/**
	 * Main, starts the Spring Application, which then calls the run method.
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(CgipriademoApplication.class, args);		
	}

	/**
	 * Runs the program
	 * @param args used for paths, {@code args[0]} is in-file path and {@code args[1]} is out-file path.
	 */
	@Override
	public void run(String... args) throws Exception {
		ApplicationData application;
		String readPath = "./maaalad.csv";
		String writePath = "./maaalad_out.csv";
		
		if(args.length > 0)
			readPath = args[0];
			
		application = applicationService.readApplicationData(readPath);	
		
		if(application == null)
			return;
		
		try {
			application = applicationService.process(application);			
		}catch(IllegalStateException e) {
			System.out.println(e.getMessage());
			return;
		}
		
		if(args.length > 1)
			writePath = args[1];

		applicationService.writeApplicationData(writePath, application);	
		System.out.println("Wrote file to " + writePath);
	}
	
}
