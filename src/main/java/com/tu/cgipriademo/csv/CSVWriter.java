package com.tu.cgipriademo.csv;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;


/**
 * Functionality to write csv files
 * @author Tambet
 *
 */
public class CSVWriter {
	
	/**
	 * Header values of the csv file
	 * 
	 */
	public static enum headerValues{
		ID,
		POLD_NR,
		MAAALA_NR,
		MAAALA_POLLUD,	
		MAAALA_KYLGNEVAD_POLLUD,
		POLD_PINDALA,
		AKTIIVNE,
		MAAALA_POLLUD_TAOTLUSEL
	}
	
	/**
	 * Writes applicationData to file
	 * @param path path to the file to write
	 * @param applicationData
	 */
	public void write(String path, List<List<String>> applicationData) {
		
		
		CSVFormat csvFormat = CSVFormat.Builder
				.create()
				.setDelimiter(';')
				.setHeader(headerValues.class)
				.build();
		try(
			BufferedWriter writer = Files.newBufferedWriter(Paths.get(path));
			CSVPrinter printer = new CSVPrinter(writer, csvFormat);
			)
		{
			for(List<String> record : applicationData) {
				printer.printRecord(record);
			}
		} catch (IOException e) {
			System.out.print("Failed to write file");
			}
		}
	}

