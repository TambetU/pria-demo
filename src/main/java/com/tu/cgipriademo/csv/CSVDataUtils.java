package com.tu.cgipriademo.csv;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.csv.CSVRecord;

import com.tu.cgipriademo.fields.Field;
import com.tu.cgipriademo.fields.FieldNr;
import com.tu.cgipriademo.fields.ApplicationData;

/**
 * Utility methods to parse/convert CSV data. 
 * All string to number conversions throw {@code NumberFormatException} if string format is not correct
 * @author Tambet
 *
 */
public class CSVDataUtils {
	/**
	 * Parses data from csv (csvRecords) to applicationData
	 * @param csvRecords data to parse
	 * @return {@code ApplicationData} parsed application
	 */
	public static ApplicationData parseFieldsData(Iterable<CSVRecord> csvRecords){
		if(csvRecords == null)
			throw new IllegalArgumentException();
		
		ApplicationData applicationData = new ApplicationData();
		
		for(CSVRecord csvRecord : csvRecords) {
			applicationData.addField(parseFieldData(csvRecord));
		}
		
		return applicationData;
	}
	
	/**
	 * Converts csv line (CSVRecord) to Field
	 * @param {@code CSVRecord} csvRecord
	 * @return {@code Field}
	 */
	public static Field parseFieldData(CSVRecord csvRecord) {
		Field field = new Field(
				Long.parseLong(csvRecord.get(CSVReader.headerValues.ID)),
				FieldNr.fromString(csvRecord.get(CSVReader.headerValues.POLD_NR)), 
				parseAreaNr(csvRecord.get(CSVReader.headerValues.MAAALA_NR)), 
				parseFieldIDList(csvRecord.get(CSVReader.headerValues.MAAALA_POLLUD)), 
				parseFieldIDList(csvRecord.get(CSVReader.headerValues.MAAALA_KYLGNEVAD_POLLUD)), 
				Float.parseFloat(csvRecord.get(CSVReader.headerValues.POLD_PINDALA).replace(',', '.')), 
				Boolean.parseBoolean(csvRecord.get(CSVReader.headerValues.AKTIIVNE))
			);
		
		
		
		return field;
	}
	
	/**
	 * Parses string that contains list of field IDs separated by ',' to {@code List<Long>} of fieldIDs
	 * @param fieldIDs {@code String}
	 * @return {@code List<long>} parsed list of values
	 */
	public static List<Long> parseFieldIDList(String fieldIDs){
		List<Long> fieldIDList = new ArrayList<Long>();	
		
		if(fieldIDs.isBlank())
			return fieldIDList;
		
		String[] idStrings = fieldIDs.split(",");
			
		
		for(String id : idStrings) {
			fieldIDList.add(Long.parseLong(id.trim()));
		}
		
		return fieldIDList;
	}
	
	
	/**
	 * Converts areaNr formatted as string to integer
	 * @param areaNr
	 * @return parsed areaNr or -1 if string is empty
	 */
	public static int parseAreaNr(String areaNr) {
		if(areaNr.isBlank())
			return -1;
		
		return Integer.parseInt(areaNr);
	}
	
	/**
	 * Converts areaNr integer to string
	 * @param areaNr
	 * @return
	 */
	public static String areaNrToString(int areaNr) {
		if(areaNr == -1)
			return "";
		
		return Integer.toString(areaNr);
	}
	
	/**
	 * Converts fieldIDs to string
	 * @param {@code List<Long>} fieldIDs
	 * @return {@code String} concatenated list of IDs separated by ","
	 */
	public static String fieldIDstoString(List<Long> fieldIDs) {
		return fieldIDs.stream().map(Object::toString).collect(Collectors.joining(","));		
	}	
	
	/**
	 * Converts ApplicationData object to list of records
	 * @param {@code ApplicationData} applicationData
	 * @return {@code List<List<String>>} list of records, where each record is list of strings
	 */
	public static List<List<String>> applicationDataToRecords(ApplicationData applicationData) {
		List<List<String>> recordList = new ArrayList<List<String>>();
		applicationData.getValues().forEach(x -> recordList.add(fieldToRecord(x)));	
		return recordList;
	}
	
	/**
	 * Converts single field to record (csv line)
	 * @param field
	 * @return record as list of strings
	 */
	public static List<String> fieldToRecord(Field field){
		return Arrays.asList(
					Long.toString(field.getID()),
					field.getFieldNr().toString(),
					areaNrToString(field.getAreaNr()),
					fieldIDstoString(field.getAreaFields()),
					fieldIDstoString(field.getFieldNeighbours()),					
					Float.toString(field.getFieldSize()).replace(".", ","), 
					Boolean.toString(field.isActive()).toUpperCase(),
					fieldNrsToString(field.getAreaFieldNrsOnApplication())
				);
	}
	
	
	/**
	 * Converts field numbers to string
	 * @param fieldNrs
	 * @return string that contains concatenated fieldNrs separated by ","
	 */
	public static String fieldNrsToString(List<FieldNr> fieldNrs) {
		return fieldNrs.stream().map(Object::toString).collect(Collectors.joining(","));
	}
}
