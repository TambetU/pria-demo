package com.tu.cgipriademo.csv;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.CSVFormat;

/**
 * Functionality to read csv files
 * @author Tambet
 *
 */
public class CSVReader {
	
	/**
	 * Header values of the csv file
	 * 
	 */
	public static enum headerValues{
		ID,
		POLD_NR,
		MAAALA_NR,
		MAAALA_POLLUD,
		MAAALA_KYLGNEVAD_POLLUD,
		POLD_PINDALA,
		AKTIIVNE,		
	}
	
	/**
	 * Reads csv file
	 * @param pathToCSV path to the file to read
	 * @return
	 */
	public Iterable<CSVRecord> read(String pathToCSV){
		
		CSVFormat csvFormat = CSVFormat.Builder
				.create()
				.setDelimiter(';')
				.setHeader(headerValues.class)
				.setAllowMissingColumnNames(false)
				.setSkipHeaderRecord(true)
				.setTrim(true)
				.build();

		try (Reader reader = Files.newBufferedReader(Paths.get(pathToCSV));
			CSVParser csvParser = new CSVParser(reader, csvFormat);) {
			Iterable<CSVRecord> csvRecords = csvParser.getRecords();
			return csvRecords;
		
		} catch (FileNotFoundException e) {
			System.out.println("File not found: " + pathToCSV);
			return null;
		}
		catch (IOException e) {
			System.out.println("Failed to read file" + e.getMessage());
			return null;
		}		
	}
	
	
}
