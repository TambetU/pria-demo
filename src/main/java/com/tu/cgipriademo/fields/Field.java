package com.tu.cgipriademo.fields;

import java.util.List;


/**
 * Represents field (<i>põld</i>) and one row on the application (<i>taotlus</i>)
 * @author Tambet
 *
 */
public class Field {
	
	private final long ID;
	private final FieldNr fieldNr;
	private final int areaNr;
	private final List<Long> areaFields;
	private final List<Long> fieldNeighbours;
	private final float fieldSize;
	private final boolean active;
	private List<FieldNr> areaFieldNrsOnApplication;		
	
	
	public Field(long iD, FieldNr fieldNr, int areaNr, List<Long> areaFields, List<Long> fieldNeighbours,
			float fieldSize, boolean active) {
		
		this.ID = iD;
		this.fieldNr = fieldNr;
		this.areaNr = areaNr;
		this.areaFields = areaFields;
		this.fieldNeighbours = fieldNeighbours;
		this.fieldSize = fieldSize;
		this.active = active;
		this.areaFieldNrsOnApplication = null;
	}
	
	public long getID() {
		return ID;
	}

	
	/**
	 * Field number (<i>Põllu number</i>)
	 * @return
	 */
	public FieldNr getFieldNr() {
		return fieldNr;
	}
	
	/**
	 * Area number (<i>Maaala number</i>)
	 * if not defined, returns {@literal -1}
	 * @return {@code int} 
	 */
	public int getAreaNr() {
		return areaNr;
	}

	/**
	 * Area fields (<i>Maaala põllud</i>)
	 * @return {@code List<Long>}
	 */
	public List<Long> getAreaFields() {
		return areaFields;
	}

	/**
	 * ID-s of field's neighbours (<i>Põlluga külgnevad põllud</i>)
	 * 
	 * @return {@code List<Long>}
	 */
	public List<Long> getFieldNeighbours() {
		return fieldNeighbours;
	}
	
	/**
	 * Field size (<i>Põllu pindala</i>)
	 * @return
	 */
	public float getFieldSize() {
		return fieldSize;
	}

	/**
	 * Field size (<i>Põllu pindala</i>)
	 * @return
	 */
	public boolean isActive() {
		return active;
	}
	
	/**
	 * Check if field has neighbours
	 * @return
	 */
	public boolean hasNeighbours() {		
		return this.fieldNeighbours.size() != 0;
	}	
	
	/**
	 * Area fields on application (<i>Maaala põllud taotlusel</i>)
	 * @return {@code List<FieldNr>}
	 */
	public List<FieldNr> getAreaFieldNrsOnApplication() {
		return areaFieldNrsOnApplication;	
	}
	
	public void setAreaFieldNrsOnApplication(List<FieldNr> areaFieldNrs) {
		this.areaFieldNrsOnApplication = areaFieldNrs;
	}		
		
}
