package com.tu.cgipriademo.fields;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.springframework.stereotype.Component;

/**
 * Functionality to check the requirements and find suitable fields that can form a land area
 * @author Tambet
 *
 */
@Component
public class ApplicableFieldsService {
	
	/**
	 * Minimum size of the field that satisfies field size requirement for land area.
	 */
	static final float AREA_LIMIT = 0.3f;
	
	
	/**
	 * For a given field, finds fields (applicable fields) that satisfy the requirements for the creation of land area. 
	 * 
	 * requirements are:
	 *  field itself must be active;
	 *  field itself must have area number;
	 *  each field in applicable fields must:
	 *  	be connected to all other fields and the original field;
	 *  	be active;
	 *  	satisfy the size requirement {@link checkFieldSizeRequirement(Collection, ApplicationData)}
	 *  
	 * @param field {@code Field} field whose land area fields to find
	 * @param applicationData {@code ApplicationData} data to check the requirements on
	 * @return {@code Collection<Long>} IDs of the fields that satisfy the requirements
	 * @throws IllegalStateException When land area fields {@code Field.getAreaFields()} or land area numbers {@code Field.getAreaNr()} are invalid
	 */

	public Collection<Long> findApplicableAreaFields(Field field, ApplicationData applicationData) {
		Collection<Long> applicableFields = new HashSet<Long>();
		
		if(!field.isActive())
			return applicableFields;
		
		if(field.getAreaNr() == -1)
			return applicableFields;			
						
		for(long fieldId : field.getAreaFields()) {
			if(!applicationData.hasKey(fieldId)) {
				throw new IllegalStateException("Land area fieldIDs contains invalid value. ID: " + fieldId);
			}
			if(!applicationData.get(fieldId).isActive())
				continue;
					
			applicableFields.add(fieldId);			
		}		
		
		FieldsAdjacencyChecker adjChecker = new FieldsAdjacencyChecker(applicationData);
		List<Long> invalidFields = adjChecker.checkFieldsAdjacency(field.getID(), applicableFields);
				
		applicableFields.removeAll(invalidFields);
		applicableFields.add(field.getID());
		applicableFields = this.checkFieldSizeRequirement(applicableFields, applicationData);
				
		return applicableFields;		
	}
	
	/**
	 * Converts list of field ids to list of corresponding fieldNrs
	 * @param applicableFields {@code Collection<Long>} list of ids
	 * @param applicationData {@code ApplicationData} data to read the fieldNrs from
	 * @return {@code List<FieldNr>} list of fieldNRs
	 */
	public List<FieldNr> fieldIdsToFieldNrs(Collection<Long> applicableFields, ApplicationData applicationData) {
		List<FieldNr> fieldNrs = new ArrayList<FieldNr>();
		applicableFields.forEach(fieldID -> fieldNrs.add(applicationData.get(fieldID).getFieldNr()));
		return fieldNrs;		
	}
	
	/**
	 * Checks the requirement for land area field sizes and returns those which satisfy the requirement
	 * Requirement for size is
	 * Land area must have a field whose size is > 0.3
	 * If all field sizes in land area are > 0.3, non should be in
	 * 
	 * @param applicableFields {@code applicableFields} fields that are checked if they can form one land area
	 * @param applicationData {@code ApplicationData} data to check the requirement on
	 * @return {@code Collection<Long>} list of Fields that satisfy the size requirement
	 */
	public Collection<Long> checkFieldSizeRequirement(Collection<Long> applicableFields, ApplicationData applicationData){
		int countBiggerThanLimit = 0;
		
		for(long fieldId : applicableFields) {
			if(applicationData.get(fieldId).getFieldSize() > AREA_LIMIT) {					
				countBiggerThanLimit++;		
			}
		}
		
		if(countBiggerThanLimit > 0 && countBiggerThanLimit < applicableFields.size()) {
			return applicableFields;
		}
		
		return new HashSet<Long>();		
	}
	
}
