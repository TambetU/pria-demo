package com.tu.cgipriademo.fields;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents data on the application (<i>taotlus</i>) as a map ((Long) ID -> (Field) field) 
 * 
 * @author Tambet
 *
 */
public class ApplicationData{
	
	private Map<Long, Field> fieldsMap;
	
	public ApplicationData() {
		this.fieldsMap = new HashMap<Long,Field>();	
	}
	
	/**
	 * Adds new field to the map
	 * 
	 * @param field {@code Field} to add
	 */
	public void addField(Field field){
		this.fieldsMap.put(field.getID(), field);
	}
	
	/**
	 * Returns {@code Field} from the map by key id
	 * @param id {@code long} key
	 * @return {@code Field} field mapped by key
	 */	
	public Field get(long id) {		
		return this.fieldsMap.get(id);
	}
	
	/**
	 * Check if map contains key
	 * @param key {@code Long} key to look for
	 * @return
	 */
	public boolean hasKey(long key) {
		return this.fieldsMap.containsKey(key);
	}
	
	/**
	 * Returns {@code Iterable} view of the values in the map
	 * @return
	 */
	public Iterable<Field> getValues() {		
		return this.fieldsMap.values();
	}
	
	
}
