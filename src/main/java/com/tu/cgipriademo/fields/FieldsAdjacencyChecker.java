package com.tu.cgipriademo.fields;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;


/**
 * Functionality to check if fields make up one continuous piece of land
 * @author Tambet
 *
 */
public class FieldsAdjacencyChecker {
	
	private ApplicationData applicationData;
	
	/**
	 * 
	 * @param applicationData data to check the adjacency on
	 */
	public FieldsAdjacencyChecker(ApplicationData applicationData) {
		this.applicationData = applicationData;
	}

	/**
	 * Checks the adjacency from field mapped by key {@code fieldID} to all the fields mapped by keys in areaFieldIds 
	 * @param fieldId {@code long} id of the field to check from
	 * @param areaFieldIds {@code Collection<Long>} id-s of the fields to check connection 
	 * @return list of IDs that are not connected to the field mapped by key fieldId
	 */
	public List<Long> checkFieldsAdjacency(long fieldId, Collection<Long> areaFieldIds) {
		
		List<Long> invalid = new ArrayList<Long>();
		
		for(long areaFieldId : areaFieldIds) {				
			if(!checkAdjecency(fieldId, areaFieldId, areaFieldIds)) {
				
				invalid.add(areaFieldId);
 		    }			
		}
		
		return invalid;
	}

	/**
	 * Checks if two fields are directly or through other fields adjacent. 
	 * Uses breadth-first search.
	 * @param a {@code long} id of the field 1
	 * @param b {@code long} id of the field 2
	 * @param applicableFields {@code Collection<Long>} IDs of fields that can be used to connect
	 * @return {@code boolean} true if connected else false
	 */
	public boolean checkAdjecency(long a, long b, Collection<Long> applicableFields) {
		HashSet<Long> checked = new HashSet<Long>();
		Queue<Long> toCheck = new LinkedList<Long>();
		
		toCheck.add(a);
		checked.add(a);
		
		while(!toCheck.isEmpty()){
			a = toCheck.poll();
			
			if(!this.applicationData.get(a).hasNeighbours())
				continue;		
			
					
			for(long neighbId : this.applicationData.get(a).getFieldNeighbours()) {
							
				if(!applicableFields.contains(neighbId)) {
					continue;
				}
				
				if(neighbId == b) {
					return true;
				}
				
				if(!checked.contains(neighbId))
					toCheck.add(neighbId);
				
				checked.add(neighbId);				
			}
		}
		
		return false;
		
	}
}
