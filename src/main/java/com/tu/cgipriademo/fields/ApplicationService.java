package com.tu.cgipriademo.fields;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tu.cgipriademo.csv.CSVDataUtils;
import com.tu.cgipriademo.csv.CSVReader;
import com.tu.cgipriademo.csv.CSVWriter;

@Service
/**
 * Service that provides functionality to read, write and process applications
 * <i>taotlus</i>
 * 
 * @author Tambet
 *
 */
public class ApplicationService {

	@Autowired
	ApplicableFieldsService applicableFieldsService;

	/**
	 * Reads data from csv file and converts data to {@link ApplicationData}
	 * @param pathToCSV path to the csv file
	 * @return data object containing the information read from the csv
	 */
	public ApplicationData readApplicationData(String pathToCSV){
		CSVReader csvReader = new CSVReader();	
		
		try {
			ApplicationData applicationData = CSVDataUtils.parseFieldsData(csvReader.read(pathToCSV));
			return applicationData;	
		}
		catch(IllegalArgumentException e){
			System.out.println("Failed to parse the file \n" + e.getMessage());
			return null;
		}
			
	}

	/**
	 * Processes (checks the requirements for land area and adds suitable fieldNrs to application)
	 * Does not create a copy of applicationData, modifies original.
	 * 
	 * @param applicationData {@code ApplicationData} application to process
	 * @return {@code ApplicationData} processed application
	 */
	public ApplicationData process(ApplicationData applicationData) {

		for (Field field : applicationData.getValues()) {
			Collection<Long> applicableFields = applicableFieldsService.findApplicableAreaFields(field,
					applicationData);

			if (applicableFields == null)
				continue;

			List<FieldNr> fieldNrs = applicableFieldsService.fieldIdsToFieldNrs(applicableFields, applicationData);
		
			Collections.sort(fieldNrs);
			field.setAreaFieldNrsOnApplication(fieldNrs);

		}

		return applicationData;
	}

	/**
	 * Writes application data to csv file.
	 * 
	 * @param pathToCSV {@code String} path to write
	 * @param applicationData {@ApplicationData} data to write
	 */
	public void writeApplicationData(String pathToCSV, ApplicationData applicationData) {
		CSVWriter csvWriter = new CSVWriter();
		csvWriter.write(pathToCSV, CSVDataUtils.applicationDataToRecords(applicationData));
	}

}
