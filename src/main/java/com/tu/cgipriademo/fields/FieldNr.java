package com.tu.cgipriademo.fields;

/**
 * Represents field nr (<i>Põllu number</i>)
 * Defined by two integers primary and secondary.
 * 
 * @author Tambet
 *
 */
public class FieldNr implements Comparable<FieldNr>{
	
	static final String SEPARATOR = "-";
	
	private final int primaryNr;
	private final int secondaryNr;
	
	/**
	 * Creates new field number from primary and secondary parts of the field number
	 * @param primary {@code int} primary part
	 * @param secondary {@code int} secondary part
	 */
	public FieldNr(int primary, int secondary) {
		this.primaryNr = primary;
		this.secondaryNr = secondary;
	}
	
	/**
	 * Creates a new field from {@code String}.
	 * @param areaNrString {@code String} to parse. It should have format like "[primary]" or "[primary]-[secondary]"
	 * @return {@code FieldNr} result or {@code null} if input string is blank
	 * @throws NumberFormatException when {@code parseInt} can't parse
	 * @throws IllegalArgumentException when areaNrString is empty
	 */
	public static FieldNr fromString(String areaNrString){
		if(areaNrString.isBlank())
			throw new IllegalArgumentException("String areaNrString must not be empty");
		
		
		String[] parts = areaNrString.split(SEPARATOR);		
		int primary = Integer.parseInt(parts[0]);	
		int secondary = parts.length == 2 ? Integer.parseInt(parts[1]) : -1;
		
		return new FieldNr(primary, secondary);
	}	
	
	
	@Override
	/**
	 * @return {@code String} representation of this fieldNr in the format [primary] or [primary]-[secondary] e.g. "1" or "1-1"
	 */
	public String toString() {
		if(secondaryNr == -1) 
			return Integer.toString(primaryNr);
		
		return primaryNr + "-" + secondaryNr;
	}

	/**
	 * 
	 * @return {@code Integer.compare} on primary and secondary with another fieldNr with primary having priority
	 * 
	 */
	@Override
	public int compareTo(FieldNr other) {
		if (this.primaryNr == other.primaryNr)
            return Integer.compare(this.secondaryNr, other.secondaryNr);        
        return Integer.compare(this.primaryNr, other.primaryNr);
	}	
}
