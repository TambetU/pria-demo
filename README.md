### Usage
   1. Clone the repo
   ```sh
   git clone https://gitlab.com/TambetU/pria-demo.git
   ```
   2. Open the project in IDE
   3. Run (when no arguments are provided, uses default paths shown below)
   
   Or
   
   1. Download the executable /bin/cgipriademo.jar
   2. Run the executable

   ```sh
   java -jar cgipriademo.jar csv_in_path csv_out_path
   ```

   e.g:
   ```sh
   java -jar cgipriademo.jar ".\maaalad.csv" ".\maaalad_out.csv"
   ```   

	csv_in_path defaults to ".\maaalad.csv"

	csv_out_path defaults to ".\maaalad_out.csv"
